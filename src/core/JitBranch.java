package core;

import gitobject.Blob;
import repository.Repository;
import repository.WorkTree;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

public class JitBranch {

    public JitBranch() {

    }
    public static void newBranch(String branchName){
    File ref_heads_branch =new File(WorkTree.getWorkTree()+ File.separator+".jit\\refs\\heads");
    if(!ref_heads_branch.exists())
        ref_heads_branch.mkdirs();
    File master=new File(ref_heads_branch+File.separator+branchName);

    if(!master.exists()) {
        try {
            master.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    }
    public static void showBranches() {
        File headsDir=new File(WorkTree.getWorkTree()+File.separator+".jit\\refs\\heads");
        Blob blob= null;
        try {
            blob = new Blob(new File(WorkTree.getWorkTree() + File.separator + ".jit\\HEAD"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        int index=blob.getValue().lastIndexOf("/");
        String tempBranch=blob.getValue().substring(index+1).trim();

        File[] fileList=headsDir.listFiles();
        for(var file:fileList){
            if(file.getName().equals(tempBranch))
                System.out.println("*"+file.getName());
            else System.out.println(file.getName());
        }
    }

    public static int branchExist(String branchName){
        File headsDir=new File(WorkTree.getWorkTree()+File.separator+".jit\\refs\\heads");
        File[] fileList=headsDir.listFiles();
        int index=0;
        for(; index< Objects.requireNonNull(fileList).length; index++){
            if(fileList[index].getName().equals(branchName));
                return index;
        }
        return -1;
    }
    public static void deleteBranch(String branchName){
        if(branchExist(branchName)==-1)
            System.out.println("Branch to be deleted does not exist");
        File headsDir=new File(WorkTree.getWorkTree()+File.separator+".jit\\refs\\heads");
        File[] fileList=headsDir.listFiles();
        int index=0;
        for(; index< Objects.requireNonNull(fileList).length; index++){
            if(fileList[index].getName().equals(branchName))
                fileList[index].delete();
        }

    }

    public static void checkout(String branchName){
        File f=new File(WorkTree.getWorkTree()+ File.separator+".jit\\HEAD");
        try (FileWriter output=new FileWriter(f,false);){
            output.write("ref: refs/heads/"+branchName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void b_checkout(String branchName) {
        newBranch(branchName);
        checkout(branchName);

    }
}
