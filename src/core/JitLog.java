package core;

import gitobject.Commit;
import gitobject.GitObject;
import repository.Repository;
import repository.WorkTree;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.Objects;

public class JitLog {
    public static void log() throws IOException, ClassNotFoundException {

        File HEAD = new File(WorkTree.getWorkTree()+ File.separator+".jit\\HEAD");
        String path = GitObject.getValue(HEAD).substring(5).replace("\r\n", "");
        System.out.println(path);
        //File branchFile = new File( "C:\\Users\\28324\\Desktop\\SimpleGit"+ File.separator + path);
        File branchFile = new File(WorkTree.getWorkTree()+ File.separator+".jit\\refs\\heads\\master");
        String newest_commit=GitObject.getValue(branchFile).substring(0,40);
        System.out.println(newest_commit);
        newest_commit="b4a1af54b8e7e8e6c6649aafaf0a6bf6ead6c518";
        Commit commit=Commit.deserialize(newest_commit,WorkTree.getWorkTree()+ File.separator+".jit\\objects");

        //用链表存成commit链
        LinkedList<Commit> commit_list=new LinkedList<>();
        //commit_list.add(commit);
        if(commit.getParent()!=null){
            do {
                //String next_commit=commit.getParent().substring(0,40);
                //为找出错误，commitid出错
                String next_commit="b4a1af54b8e7e8e6c6649aafaf0a6bf6ead6c518";
                commit= Commit.deserialize(next_commit,WorkTree.getWorkTree()+ File.separator+".jit\\objects");
                commit_list.add(commit);

            }while (!Objects.equals(commit.getParent(), ""));
        }

        for(Commit c: commit_list){
            String[] list = c.getAuthor().split(" ");
            String author = list[0];
            //Date date = new Date(Long.parseLong(list[1]));
            Date date=new Date();
            System.out.println("commit:  " + c.getKey());
            System.out.println("Author:  " + author);
            System.out.println("Date:    " + date + " " + list[2]);
            System.out.println("message:    " + c.getMessage());
        }

    }
}
