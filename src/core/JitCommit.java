package core;

import fileoperation.FileCreation;
import gitobject.Blob;
import gitobject.Commit;
import gitobject.Tree;
import repository.WorkTree;

import java.io.*;
import java.net.InetAddress;
import java.util.Date;

public class JitCommit {

    public static boolean commit(String message)throws IOException {
        FileInputStream in = new FileInputStream(WorkTree.getWorkTree()+ File.separator+".jit\\index");
        BufferedReader buffer = new BufferedReader(new InputStreamReader(in));
        String blobString;
        while((blobString = buffer.readLine())!=null) {
            if(blobString.length()<40)
                continue;
            String[] list = blobString.split(" ");
            Blob blob = null;
            try {
                blob = Blob.deserialize(list[1]);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            String filePath =  WorkTree.getWorkTree()+ File.separator+".jit\\temp" + File.separator + list[3];
            int index = filePath.lastIndexOf('\\');
            String dirPath = filePath.substring(0, index);
            String filename = filePath.substring(index+1);
            File file = new File(dirPath);
            if (!file.exists()) {
                file.mkdirs();
            }
            File newfile = new File(dirPath+File.separator+filename);
            System.out.println(newfile);
            if(!newfile.exists()) {
                newfile.createNewFile();
            }

            FileWriter fileWriter = new FileWriter(newfile);
            fileWriter.write(blob.getValue());

            fileWriter.close();
        }

        in.close();
        buffer.close();

        File file = new File(WorkTree.getWorkTree()+ File.separator+".jit\\temp");
        Tree tree = null;
        try {
            tree = new Tree(file);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String hashRes = tree.getKey();
        if (hashRes.length() != 40) {
            throw new IOException("hash value length error!");
        }
        //bug，先试试绝对路劲
        //String parentPath = tree.getPath();//Tree类getPath()需要借助Repository类的加载
        String parentPath=WorkTree.getWorkTree()+ File.separator+".jit\\objects";

        File check_directory_exist = new File( parentPath+ File.separator + hashRes.substring(0, 2));
        if (!check_directory_exist.exists()) {
            FileCreation.createDirectory(parentPath, hashRes.substring(0, 2));
        }

        File check_tree_exist = new File(parentPath + File.separator + hashRes.substring(0, 2) + File.separator + hashRes.substring(2));
        if (!check_tree_exist.exists()) {
            FileCreation.createFile( WorkTree.getWorkTree()+ File.separator+".jit\\objects\\" +hashRes.substring(0, 2), hashRes.substring(2), null);
            String path = WorkTree.getWorkTree()+ File.separator+".jit\\objects\\" + hashRes.substring(0, 2) + File.separator + hashRes.substring(2);
            try {
                tree.compressWrite(path, tree);//对象序列化,压缩后写入.jit/objects
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //FileDeletion.deleteFile(file);


        InetAddress addr = InetAddress.getLocalHost();
        Date date = new Date();
        String hostname = addr.getHostName() + " " + date + " +0800";
        Commit commit = null;
        try {
            commit = new Commit(tree, hostname, hostname, message);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assert commit != null;

        String hashres = commit.getKey();
        String dir=WorkTree.getWorkTree()+ File.separator+".jit\\objects";
        File ref_heads_branch =new File(WorkTree.getWorkTree()+ File.separator+".jit\\refs\\heads");
        if(!ref_heads_branch.exists())
            ref_heads_branch.mkdirs();
        String branchName;
        File heads_branchName=new File(WorkTree.getWorkTree()+ File.separator+".jit\\HEAD");
        Blob branchBlob= null;
        try {
            branchBlob = new Blob(heads_branchName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        int index=branchBlob.getValue().lastIndexOf("/");
        branchName=branchBlob.getValue().substring(index+1);

        File master=new File(ref_heads_branch+File.separator+branchName.trim());
        //System.out.println(master.equals("C:\\Users\\28324\\Desktop\\SimpleGit\\.jit\\refs\\heads\\master"));
        if(!master.exists())
           master.createNewFile();
        //FileCreation.createFile(String.valueOf(ref_heads_branch),branchName,null);

        Blob blob= null;
        try {
            blob = new Blob(new File(WorkTree.getWorkTree() + File.separator + ".jit\\HEAD"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        int indexOfBranch=blob.getValue().lastIndexOf("/");
        String tempBranch=blob.getValue().substring(index+1).trim();
        FileWriter fileWriter = new FileWriter(WorkTree.getWorkTree()+File.separator+".jit\\refs\\heads\\"+tempBranch);
        fileWriter.write(hashres);

        fileWriter.close();
        check_directory_exist = new File(dir + File.separator + hashres.substring(0, 2));
        if (!check_directory_exist.exists()) {
            FileCreation.createDirectory(dir, hashres.substring(0, 2));
        }

        File check_commit_exist = new File(dir + File.separator + hashres.substring(0, 2) + File.separator + hashres.substring(2));
        if (!check_commit_exist.exists()) {
            FileCreation.createFile(dir + File.separator + hashres.substring(0, 2), hashres.substring(2), null);
            String path = dir + File.separator + hashRes.substring(0, 2) + File.separator + hashRes.substring(2);
            try {

                commit.compressWrite(path, commit);//对象序列化,压缩后写入.jit/objects
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //commit之后清除index中内容
        File indexFile=new File(WorkTree.getWorkTree()+File.separator+".jit\\index");
        PrintWriter writer = new PrintWriter(indexFile);
        writer.print("");
        writer.close();
        /*try {

            Commit newcommit = new Commit( new Tree(), author, committer, s.substring(1));
            newcommit.compressWrite(Repository.getGitDir()+"objects",commit);
            commitList.add(0,commit);

            indexList.clear();
            JitHash.hash(String.valueOf(commit));
        } catch (Exception e) {
            e.printStackTrace();
        }
        File f=new File("C:\\Users\\28324\\Desktop\\SimpleGit\\.jit\\index");
        try (FileWriter output=new FileWriter(f,false);){
            output.write("");
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Commit successfully.");*/

        //删除临时文件夹temp
        // File tempFile=new File(WorkTree.getWorkTree() + File.separator + ".jit\\temp");
        //tempFile.delete();
        return true;
    }
}
