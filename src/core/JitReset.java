package core;

import fileoperation.FileCreation;
import fileoperation.FileDeletion;
import fileoperation.FileReader;
import gitobject.Blob;
import gitobject.Commit;
import gitobject.GitObject;
import gitobject.Tree;
import repository.Repository;
import repository.WorkTree;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class JitReset {
    public static void reset(String mode,String commitId) throws IOException, ClassNotFoundException {
        String repoPath= WorkTree.getWorkTree();
        Commit check_exist = Commit.deserialize(commitId);
        if (check_exist == null) {
            throw new IOException("Invalid reset operation");
        }
         if(mode.equals("--soft")) {
                File HEAD = new File(repoPath + File.separator + "HEAD");
                String path = GitObject.getValue(HEAD).substring(5).replace("\r\n", "");
                File branchFile = new File(Repository.getGitDir() + File.separator + path);
                FileWriter fileWriter = new FileWriter(branchFile);
                fileWriter.write(commitId);
                fileWriter.close();
            }
         else
             if(mode.equals("--mixed")){
                 Commit commit = Commit.deserialize(commitId);
                 Tree tree = Tree.deserialize(commit.getTree());
                 File file = new File(repoPath + File.separator + "index");
                 if(file.exists()) {
                     file.delete();
                 }
                 file.createNewFile();
            }
        else
            if(mode.equals("--hard")){
            Commit commit = Commit.deserialize(commitId);
            Tree tree = Tree.deserialize(commit.getTree());
            int index = repoPath.lastIndexOf('\\');
            String workDirectory = repoPath.substring(0, index);
            File file = new File(workDirectory);
            File[] fs = file.listFiles();
            for(File f : fs) {
                if(!f.getName().equals(".jit")) {
                    FileDeletion.deleteFile(f);
                }
            }
            recoverWorkTree(tree, workDirectory);

        }
    }
    public static void recoverWorkTree(Tree t, String parentTree) throws IOException {
        ArrayList<String> list = FileReader.readByBufferReader(t.getValue());
        ArrayList<GitObject> treeList = t.getTreeList();
        Iterator iterator = treeList.iterator();
        boolean isRootDir = true;

        for (String s : list) {

            if (FileReader.readObjectFmt(s).equals("blob")) {
                String fileName = FileReader.readObjectFileName(s);
                Blob blob = (Blob)iterator.next();
                FileCreation.createFile(parentTree, fileName, blob.getValue());
            }
            else {
                String dirName = FileReader.readObjectFileName(s);
                if(isRootDir) {
                    isRootDir = false;
                }
                else {
                    Tree tree = (Tree)iterator.next();
                    FileCreation.createDirectory(parentTree, dirName);
                    recoverWorkTree(tree, parentTree + File.separator + dirName);
                }
            }
        }
    }
}
