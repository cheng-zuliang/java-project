package core;

import fileoperation.FileCreation;
import fileoperation.FileDeletion;
import repository.Repository;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import gitobject.*;
import repository.WorkTree;

public class JitHash {
    /**
     * Init repository in your working area.
     * @param filename
     * @throws IOException
     */


    public static void hash(String filename) throws Exception {
        /* Todo: You should pass the filename in this function, and generate a hash file in your repository.
        *   Add your code here.*/
        File file=new File(filename);
        if(file.isFile()) {
            try {
                Blob blob = new Blob(file);
                String path = WorkTree.getWorkTree()+ File.separator  + ".jit" + File.separator + "objects";
                String hashcode = blob.genKey(file);
                FileCreation.createDirectory(path, hashcode.substring(0, 2));
                FileCreation.createFile(path + File.separator + hashcode.substring(0, 2), hashcode.substring(2), null);

                String newPath=path + File.separator + hashcode.substring(0, 2)+File.separator+hashcode.substring(2);//要加separator么

                blob.compressWrite(newPath, blob);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(file.isDirectory()){
            try{

                File[] fs = file.listFiles();
                ArrayList<File> fileList = Tree.sortFile(fs);
                for (File file1 : fileList) {
                JitHash.hash(file1.getPath());
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
