package core;

import fileoperation.FileDeletion;
import gitobject.GitObject;
import repository.WorkTree;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class JitRemove {
    public static void remove(File file) throws IOException {
        File f=new File(WorkTree.getWorkTree()+ File.separator+".jit\\index");
        String string= GitObject.getValue(f);
        FileDeletion.deleteContent(f);

        int indexOfName=string.indexOf(file.getAbsolutePath().replace(WorkTree.getWorkTree()+ File.separator,""));
        int indexOfKey,indexOfBlob,indexOfEnd;

        String key;
        if(indexOfName != -1) {
            indexOfKey=indexOfName -43;
            indexOfBlob=indexOfKey-7;
            indexOfEnd=indexOfName+(file.getAbsolutePath().replace(WorkTree.getWorkTree()+ File.separator,"")).length();
            key=string.substring(indexOfKey,indexOfKey+40);


            String dir=string.substring(indexOfKey,indexOfKey+40).substring(0,2);
            String filename=string.substring(indexOfKey,indexOfKey+40).substring(2);

            //应该是把文件夹也删除吧
            /*String deletePath="C:\\Users\\28324\\Desktop\\SimpleGit\\.jit\\objects\\"+dir;
            System.out.println(deletePath);
            FileDeletion.deleteFile(deletePath);*/
            new File(WorkTree.getWorkTree()+ File.separator+".jit\\objects\\"+dir).delete();

            String newString=string.replace(string.substring(indexOfBlob,indexOfEnd),"");
            PrintWriter output = new PrintWriter(f);
            output.print(newString);
            output.close();
            System.out.println("Files removed successfully.");

        }
        else System.out.println("Files removed failed.");

    }
}
