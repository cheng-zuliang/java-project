package core;

import gitobject.Blob;
import gitobject.Commit;
import gitobject.Tree;
import repository.Repository;
import repository.WorkTree;
import stage.Index;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Scanner;

public class JitAdd {
	/**
	 * Add files to index.
	 * 'jit add' equals to two commands: 'jit hash-object<file>' and 'jit update-index --add<file>'.
	 * @param file
	 * @throws Exception
	 */

	private static String author ="czl";
	private static String committer="sj";

	private static LinkedList<String[]> indexList=new LinkedList<>();
	private static ArrayList<Blob> blobList= new ArrayList<>();
	private static ArrayList<Tree> treeList=new ArrayList<>();
	private static ArrayList<Commit> commitList=new ArrayList<>();
	private static String path = Repository.getGitDir();

    /*public Index(File file) throws Exception {
        fmt = "blob";
        mode = "100644";
        value = getValue(file);
        key=Blob.genKey(file);
        //compressWrite(path,);
    }*/
	//Add方法
    /*git hash-object <filename>
      git update-index <filename>
      运行第一条命令，git将会根据新生成的文件产生一个长度为40的SHA-1哈希字符串，
      并在.git/objects目录下生成一个以该SHA-1的前两个字符命名的子目录，然后在该子目录下，
      存储刚刚生成的一个新文件，新文件名称是SHA-1的剩下的38个字符。

      第二条命令将会更新.git/index索引，使它指向 objects 目录下新生成的文件。
      新建的文件在index中没有记录，如果也不在.gitignore中，那么git status就能检测到；
      如果修改了文件，那么文件的最后修改时间和index中的不一样，那么git status就能检测到
      */

	static boolean findFileInWorkTree(File s) {
		return true;
	}
	static boolean findDirInWorkTree(File s){
		return true;
	}
    public static boolean findFileIndex(String hashCode){
		HashSet<String> set=new HashSet<>();
		File file = new File(WorkTree.getWorkTree()+File.separator+".jit\\index");
		try {
			Scanner input = new Scanner(file);
			while(input.hasNext()){
				String line = input.next();
				set.add(line);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return set.contains(hashCode);


	}

	public static boolean addDir(File fl)throws Exception {
		File[] files=fl.listFiles();
		assert files != null;
		for(var file:files){
			//if(FileStatus.fileChanged(file.getParent(),file.getName(),indexList))
			if(file.isFile()){
				String[] str = new String[4];
				str[0]="100644";
				str[1]=new Blob(file).getKey();
				str[2]="0";
				str[3]=file.getAbsolutePath().replace(WorkTree.getWorkTree()+ File.separator,"");
				JitHash.hash(file.getPath());
				File f=new File(WorkTree.getWorkTree()+ File.separator+".jit\\index");
				try (FileWriter output=new FileWriter(f,true);){
					if(findFileIndex(str[1]))
						continue;
					for(int i = 0; i < str.length; i++){
						output.write(str[i]+" ");
					}
					output.write("\n");
				}
			}
			if(file.isDirectory()) {
				addDir(file);
			}

		}
		return true;
	}

	public static boolean add(File file) {
		try {
			blobList.add(new Blob(file));
			String[] str = new String[4];
			str[0]="100644";
			//str[1]=SHA1.getHash(file);
			str[1]=new Blob(file).getKey();
			//str[2]=String.valueOf(file.lastModified());
			//str[2]=FileStatus.fileChanged(Repository.getWorkTree()+File.separator+"test", file.getName(),indexList)==true?"1":"0";
			str[2]="0";
			str[3]=file.getAbsolutePath().replace(WorkTree.getWorkTree()+ File.separator,"");;
			indexList.add(str);
			JitHash.hash(file.getPath());
			File f=new File(WorkTree.getWorkTree()+ File.separator+".jit\\index");
			try (FileWriter output=new FileWriter(f,true);){
				for(int i = 0; i < str.length; i++){
					output.write(str[i]+" ");
				}
				output.write("\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

}

