package repository;

import fileoperation.FileCreation;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

/**
 * Todo: Add your own code. JitInit.init("worktree") should be able to create a repository "worktree/.jit" ,
 *       which contains all the default files and other repositories inside. If the repository has
 *       already exists, delete the former one and create a new one. You're welcome to reconstruct the code,
 *       but don't change the code in the core directory.
 */
public class Repository {
    private static String workTree;	//working directory
    private static String gitDir;	//jit repository path

    //private enum dir {COMMIT_EDITMSG,config,HEAD,description};

    /**
     * Constructor
     */
    public Repository() throws IOException {
        if(gitDir == ""){
            throw new IOException("The repository does not exist!");
        }
    }
    
    /**
     * Construct a new repository instance with certain path.
     * Constructor
     * @param path
     * @throws IOException
     */
    public Repository(String path) throws IOException {
        this.workTree = path;
        this.gitDir = path + File.separator + ".jit";
    }

    public static String getGitDir() {
        return gitDir;
    }

    public static String getWorkTree() {
        return workTree;
    }
    
    /**
     * Helper functions.
     * @return
     */
    public boolean exist(){ return new File(gitDir).exists(); }

    public boolean isFile(){ return new File(gitDir).isFile(); }

    public boolean isDirectory(){ return new File(gitDir).isDirectory(); }


    /**
     * Create the repository and files and directories inside.
     * @return boolean
     * @throws IOException
     */
    public void createRepo() throws IOException {
        /* Todo：Add your code here. */
        ArrayList<String> dirs=new ArrayList<>(){{
            add("hooks");
            add("info");
            add("objects");
            add("logs");
            add("refs");
            add("refs"+File.separator+"heads");
        }};
        String[] files={"COMMIT_EDITMSG","config","index","HEAD","description","refs"+File.separator+"heads"+File.separator+"master"};
        /*or(var directory:dirs){
            File file=new File(workTree+File.separator+directory);//用绝对路径还是相对路径？
            file.mkdir();
        }*/
        FileCreation.createDirectory(workTree,".jit");
        FileCreation.createDirectory(getGitDir(),dirs);
        //FileCreation.createDirectory(getGitDir()+File.separator + "refs","heads");
        //FileCreation.createFile(getGitDir()+File.separator + "refs"+"heads","master",null);
        for(var file:files){
            FileCreation.createFile(getGitDir(),file,null);
        }
        //将HEAD指针默认指向master分支
        File HEAD = new File(gitDir+File.separator+"HEAD");
        FileWriter HEADwriter = new FileWriter(HEAD);
        HEADwriter.write("ref: refs/heads/master\n");
        HEADwriter.close();
        //将当前工作区路径存入appendix文件中，防止程序中断后丢失
        FileCreation.createFile(getGitDir(), "appendix",getWorkTree());
    }
}
