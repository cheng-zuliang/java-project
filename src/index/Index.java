package index;

import java.io.*;
import java.net.InetAddress;
import java.util.*;

import core.JitHash;
import fileoperation.FileCreation;
import fileoperation.FileDeletion;
import repository.Repository;
import sha1.*;
import gitobject.*;
import fileoperation.FileStatus;
public class Index<tree> extends GitObject implements Serializable {
    private static String author ="czl";
    private static String committer="sj";

    private static LinkedList<String[]> indexList=new LinkedList<>();
    private static ArrayList<Blob> blobList= new ArrayList<>();
    private static ArrayList<Tree> treeList=new ArrayList<>();
    private static ArrayList<Commit> commitList=new ArrayList<>();
    private static String path = Repository.getGitDir();

    /*public Index(File file) throws Exception {
        fmt = "blob";
        mode = "100644";
        value = getValue(file);
        key=Blob.genKey(file);
        //compressWrite(path,);
    }*/
    //Add方法
    /*git hash-object <filename>
      git update-index <filename>
      运行第一条命令，git将会根据新生成的文件产生一个长度为40的SHA-1哈希字符串，
      并在.git/objects目录下生成一个以该SHA-1的前两个字符命名的子目录，然后在该子目录下，
      存储刚刚生成的一个新文件，新文件名称是SHA-1的剩下的38个字符。

      第二条命令将会更新.git/index索引，使它指向 objects 目录下新生成的文件。
      新建的文件在index中没有记录，如果也不在.gitignore中，那么git status就能检测到；
      如果修改了文件，那么文件的最后修改时间和index中的不一样，那么git status就能检测到
      */

    static boolean findFileInWorkTree(File s) {
        return true;
    }
    static boolean findDirInWorkTree(File s){
        return true;
    }

    public static boolean addDir(File fl)throws Exception {
        File[] files=fl.listFiles();
            assert files != null;
            for(var file:files){
                //if(FileStatus.fileChanged(file.getParent(),file.getName(),indexList))
                if(file.isFile()){
                    String[] str = new String[4];
                    str[0]="100644";
                    str[1]=new Blob(file).getKey();
                    //str[2]=String.valueOf(file.lastModified());
                    str[2]="0";
                    str[3]=file.getAbsolutePath().replace("C:\\Users\\28324\\Desktop\\SimpleGit\\","");

                    //Tree tree = new Tree();
                    /*Tree tree=new Tree(new File(Repository.getWorkTree()+File.separator + "Discrete"));
                    treeList.add(0,tree);*/

                    JitHash.hash(file.getPath());
                    File f=new File("C:\\Users\\28324\\Desktop\\SimpleGit\\.jit\\index");
                    try (FileWriter output=new FileWriter(f,true);){
                        for(int i = 0; i < str.length; i++){
                            output.write(str[i]+" ");
                        }
                        output.write("\n");
                    }
                }
                if(file.isDirectory()) {
                    addDir(file);
                }

        }
        return true;
    }

    public static boolean add(File file) {
        try {
            blobList.add(new Blob(file));
            String[] str = new String[4];
            str[0]="100644";
            //str[1]=SHA1.getHash(file);
            str[1]=new Blob(file).getKey();
            //str[2]=String.valueOf(file.lastModified());
            //str[2]=FileStatus.fileChanged(Repository.getWorkTree()+File.separator+"test", file.getName(),indexList)==true?"1":"0";
            str[2]="0";
            str[3]=file.getAbsolutePath().replace("C:\\Users\\28324\\Desktop\\SimpleGit\\","");
            indexList.add(str);
            JitHash.hash(file.getPath());
            File f=new File("C:\\Users\\28324\\Desktop\\SimpleGit\\.jit\\index");
            try (FileWriter output=new FileWriter(f,true);){
                for(int i = 0; i < str.length; i++){
                    output.write(str[i]+" ");
                }
                output.write("\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }


    //Remove方法

    public static void remove(File file) throws IOException {
        File f=new File("C:\\Users\\28324\\Desktop\\SimpleGit\\.jit\\index");
        String string=GitObject.getValue(f);
        FileDeletion.deleteContent(f);

        int indexOfName=string.indexOf(file.getAbsolutePath().replace("C:\\Users\\28324\\Desktop\\SimpleGit\\",""));
        int indexOfKey,indexOfBlob,indexOfEnd;

        String key;
        if(indexOfName != -1) {
            indexOfKey=indexOfName -43;
            indexOfBlob=indexOfKey-7;
            indexOfEnd=indexOfName+(file.getAbsolutePath().replace("C:\\Users\\28324\\Desktop\\SimpleGit\\","")).length();
            key=string.substring(indexOfKey,indexOfKey+40);


            String dir=string.substring(indexOfKey,indexOfKey+40).substring(0,2);
            String filename=string.substring(indexOfKey,indexOfKey+40).substring(2);

            //应该是把文件夹也删除吧
            /*String deletePath="C:\\Users\\28324\\Desktop\\SimpleGit\\.jit\\objects\\"+dir;
            System.out.println(deletePath);
            FileDeletion.deleteFile(deletePath);*/
            new File("C:\\Users\\28324\\Desktop\\SimpleGit\\.jit\\objects\\"+dir).delete();

            String newString=string.replace(string.substring(indexOfBlob,indexOfEnd),"");
            PrintWriter output = new PrintWriter(f);
            output.print(newString);
            output.close();

        }

    }


    //commit方法
    public static boolean commit(String message)throws IOException,ClassNotFoundException {

        FileInputStream in = new FileInputStream("C:\\Users\\28324\\Desktop\\SimpleGit\\.jit\\index");
        BufferedReader buffer = new BufferedReader(new InputStreamReader(in));
        String blobString;
        while((blobString = buffer.readLine()) != null) {
            String[] list = blobString.split(" ");
            Blob blob = null;
            try {
                blob = Blob.deserialize(list[1]);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            String filePath =  "C:\\Users\\28324\\Desktop\\SimpleGit\\.jit\\temp" + File.separator + list[3];
            int index = filePath.lastIndexOf('\\');
            String dirPath = filePath.substring(0, index);
            String filename = filePath.substring(index+1);
            File file = new File(dirPath);
            if (!file.exists()) {
                file.mkdirs();
            }
            File newfile = new File(dirPath+File.separator+filename);
            System.out.println(newfile);
            if(!newfile.exists()) {
                newfile.createNewFile();
            }

            FileWriter fileWriter = new FileWriter(newfile);
            fileWriter.write(blob.getValue());

            fileWriter.close();
        }

        in.close();
        buffer.close();

        File file = new File("C:\\Users\\28324\\Desktop\\SimpleGit\\.jit\\temp");
        Tree tree = null;
        try {
            tree = new Tree(file);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String hashRes = tree.getKey();
        System.out.println(hashRes);
        if (hashRes.length() != 40) {
            throw new IOException("hash value length error!");
        }
        //bug，先试试绝对路劲
        //String parentPath = tree.getPath();//Tree类getPath()需要借助Repository类的加载
        String parentPath="C:\\Users\\28324\\Desktop\\SimpleGit\\.jit\\objects";

          File check_directory_exist = new File( parentPath+ File.separator + hashRes.substring(0, 2));
        if (!check_directory_exist.exists()) {
            FileCreation.createDirectory(parentPath, hashRes.substring(0, 2));
        }

        File check_tree_exist = new File(parentPath + File.separator + hashRes.substring(0, 2) + File.separator + hashRes.substring(2));
        if (!check_tree_exist.exists()) {
            FileCreation.createFile( "C:\\Users\\28324\\Desktop\\SimpleGit\\.jit\\objects\\" +hashRes.substring(0, 2), hashRes.substring(2), null);
            String path = "C:\\Users\\28324\\Desktop\\SimpleGit\\.jit\\objects\\" + hashRes.substring(0, 2) + File.separator + hashRes.substring(2);
            try {
                tree.compressWrite(path, tree);//对象序列化,压缩后写入.jit/objects
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //FileDeletion.deleteFile(file);


        InetAddress addr = InetAddress.getLocalHost();
        Date date = new Date();
        String hostname = addr.getHostName() + " " + date.getTime()/1000 + " +0800";
        Commit commit = null;
        try {
            commit = new Commit(tree, hostname, hostname, message);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assert commit != null;
        
        String hashres = commit.getKey();
        System.out.println(hashres);
        String dir="C:\\Users\\28324\\Desktop\\SimpleGit\\.jit\\objects";
        File ref_heads_branch =new File("C:\\Users\\28324\\Desktop\\SimpleGit\\.jit\\refs\\heads");
        if(!ref_heads_branch.exists())
            ref_heads_branch.mkdirs();
        File master=new File(ref_heads_branch+File.separator+"master");

        if(!master.exists())
            master.createNewFile();
        FileWriter fileWriter = new FileWriter(master);
        fileWriter.write(hashres);//后续需要修改为对应分支

        fileWriter.close();
        check_directory_exist = new File(dir + File.separator + hashres.substring(0, 2));
        if (!check_directory_exist.exists()) {
            FileCreation.createDirectory(dir, hashres.substring(0, 2));
        }

        File check_commit_exist = new File(dir + File.separator + hashres.substring(0, 2) + File.separator + hashres.substring(2));
        if (!check_commit_exist.exists()) {
            FileCreation.createFile(dir + File.separator + hashres.substring(0, 2), hashres.substring(2), null);
            String path = dir + File.separator + hashRes.substring(0, 2) + File.separator + hashRes.substring(2);
            try {
                System.out.println(path);

                commit.compressWrite(path, commit);//对象序列化,压缩后写入.jit/objects
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /*try {

            Commit newcommit = new Commit( new Tree(), author, committer, s.substring(1));
            newcommit.compressWrite(Repository.getGitDir()+"objects",commit);
            commitList.add(0,commit);
            
            indexList.clear();
            JitHash.hash(String.valueOf(commit));
        } catch (Exception e) {
            e.printStackTrace();
        }
        File f=new File("C:\\Users\\28324\\Desktop\\SimpleGit\\.jit\\index");
        try (FileWriter output=new FileWriter(f,false);){
            output.write("");
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Commit successfully.");*/
        return true;
    }

    //remove方法
    public static void log() throws IOException, ClassNotFoundException {

        File HEAD = new File("C:\\Users\\28324\\Desktop\\SimpleGit\\.jit\\HEAD");
        String path = GitObject.getValue(HEAD).substring(5).replace("\r\n", "");
        System.out.println(path);
        //File branchFile = new File( "C:\\Users\\28324\\Desktop\\SimpleGit"+ File.separator + path);
        File branchFile = new File( "C:\\Users\\28324\\Desktop\\SimpleGit\\.jit\\refs\\heads\\master");
        String newest_commit=GitObject.getValue(branchFile).substring(0,40);
        System.out.println(newest_commit);
        Commit commit=Commit.deserialize(newest_commit,"C:\\Users\\28324\\Desktop\\SimpleGit\\.jit\\objects");

        //用链表存成commit链
        LinkedList<Commit> commit_list=new LinkedList<>();
        commit_list.add(commit);
        if(commit.getParent()!=null){
            do {
                String next_commit=commit.getParent().substring(0,40);
                commit= Commit.deserialize(next_commit,"C:\\Users\\28324\\Desktop\\SimpleGit\\.jit\\objects");
                commit_list.add(commit);

            }while (!Objects.equals(commit.getParent(), ""));
        }

        for(Commit c: commit_list){
            String[] list = c.getAuthor().split(" ");
            String author = list[0];
            Date date = new Date(Long.parseLong(list[1]));
            System.out.println("commit:  " + c.getKey());
            System.out.println("Author:  " + author);
            System.out.println("Date:  " + date + " " + list[2]);
            System.out.println("message:  " + c.getMessage());
        }

    }

    //reset方法
    public static void reset(String repoPath,String mode,String commitId) throws IOException, ClassNotFoundException {

        //检查commit对象是否存在
        Commit check_exist=Commit.deserialize(commitId);
        if(check_exist==null){
            throw new IOException("需要reset的commit对象不存在~~");
        }
        switch (mode) {
            case "--soft" -> {
                //获取当前分支指向的commit对象
                File HEAD = new File(repoPath + File.separator + "HEAD");
                String path = GitObject.getValue(HEAD).substring(5).replace("\r\n", "");
                File branchFile = new File(Repository.getGitDir() + File.separator + path);

                //重写commit指针
                FileWriter fileWriter = new FileWriter(branchFile);
                fileWriter.write(commitId);
                fileWriter.close();
                break;

            }
            case "--mixed" -> {
                //获取当前分支指向的commit对象
                File HEAD = new File(repoPath + File.separator + "HEAD");
                String path = GitObject.getValue(HEAD).substring(5).replace("\r\n", "");
                File branchFile = new File(Repository.getGitDir() + File.separator + path);

                //重写commit指针
                FileWriter fileWriter = new FileWriter(branchFile);
                fileWriter.write(commitId);
                fileWriter.close();

                //重置index暂存区

            }
            case "--hard" -> {

            }
        }
    }


    public void writeIndex() throws IOException {
        if(new File(path + File.separator + "index").exists()) {
            File indexFile = new File(path + File.separator + "index");
            FileWriter fw = new FileWriter(indexFile);
            for(String[] arr : indexList) {
                String str = arrayToString(arr);
                fw.write(str);
                fw.write("\n");
            }
            fw.flush();
            fw.close();
        }
    }
    public String arrayToString(String[] arr) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            stringBuilder.append(arr[i] + " ");
        }
        String res = stringBuilder.toString().substring(0, stringBuilder.length() - 1);
        return res;
    }
}
