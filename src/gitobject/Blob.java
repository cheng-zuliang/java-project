package gitobject;

import repository.WorkTree;
import sha1.SHA1;
import zlib.ZLibUtils;

import java.io.*;

public class Blob extends GitObject{
    
	public String getFmt(){
        return fmt;
    }
    public String getMode(){
        return mode;
    }
    public String getPath() {
        return path;
    }
    public String getValue(){
        return value;
    }
    public String getKey() { return key; }
    public Blob(){};
    /**
     * Constructing blob object from a file
     * @param file
     * @throws Exception
     */
    public Blob(File file) throws Exception {
        fmt = "blob";
        mode = "100644";
        value = getValue(file);
        key=genKey(file);
        //compressWrite(path,);
    }

    /**
     * Deserialize a blob object from an existed hash file in .jit/objects.
     * @param Id
     * @throws IOException
     */
    public static Blob deserialize(String Id) throws IOException, ClassNotFoundException {

        String parentPath= WorkTree.getWorkTree()+ File.separator+".jit\\objects"+File.separator+Id.substring(0,2);
        String filename=Id.substring(2);
        String path=parentPath+File.separator+filename;
        FileInputStream fileInputStream=new FileInputStream(path);
        byte[] content=ZLibUtils.decompress(fileInputStream);
        InputStream inputStream=new ByteArrayInputStream(content);

        ObjectInputStream obj=new ObjectInputStream(inputStream);

        return (Blob) obj.readObject();
    }

    /**
     * Generate key from file.
     * @param file
     * @return String
     * @throws Exception
     */
    public   String genKey(File file) throws Exception {
        /**
         * Add your code here.
         */
        key=SHA1.getHash(getValue(file));
        return key;
    }
    @Override
    public String toString(){
        return "100644 blob " + key;
    }
}
