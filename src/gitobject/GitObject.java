package gitobject;
import fileoperation.FileDeletion;
import repository.Repository;
import zlib.ZLibUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;


public class GitObject implements Serializable{

    protected String fmt;                  //type of object
    protected String key;                  //key of object
    protected String mode;                 //mode of object
    protected static String path = Repository.getGitDir() + File.separator + "objects";          //absolute path of objects
    protected String value;                //value of object

    public String getFmt(){return fmt;}
    public String getKey() { return key; }
    public String getMode(){
        return mode;
    }
    public String getPath() {
        return path;
    }
    public String getValue(){
        return value;
    }


    public GitObject(){}
    /**
     * Get the value(content) of file
     * @param file
     * @return String
     * @throws IOException
     */
    public static String getValue(File file) throws IOException {
        /* Todo: Add your code here
        *   You should delete the return statement below before coding.  */
        /*try (
            InputStreamReader input=new InputStreamReader(new FileInputStream(file));
            BufferedReader bufferedReader=new BufferedReader(input);)
        {
            String line;
            StringBuilder stringBuilder=new StringBuilder();
            line= bufferedReader.readLine();
            while(line!=null){
                stringBuilder.append(line);
                stringBuilder.append(System.lineSeparator());
                line=bufferedReader.readLine();
            }
            return String.valueOf(stringBuilder);
        }*/
        FileInputStream fileInputStream=new FileInputStream(file);
        InputStreamReader inputStreamReader=new InputStreamReader(fileInputStream);
        BufferedReader bufferedReader=new BufferedReader(inputStreamReader);
        String line;
        StringBuilder stringBuilder=new StringBuilder();
        try {
            line=bufferedReader.readLine();
            while(line!=null){
                stringBuilder.append(line);
                stringBuilder.append(System.lineSeparator());
                line=bufferedReader.readLine();
            }
            bufferedReader.close();
            inputStreamReader.close();
            fileInputStream.close();
        }catch (IOException e){
            e.printStackTrace();
        }
        return String.valueOf(stringBuilder);
    }



    /**
     * Todo: Serialize the object and compress with zlib.
     * @throws Exception
     */

    public  void compressWrite() throws Exception{
        byte[] data = ZLibUtils.compress(value.getBytes());
        FileOutputStream fos = new FileOutputStream(path + File.separator + key);
        fos.write(data);
        fos.close();
    }
    public void compressWrite(String path,GitObject gitObject) throws Exception{
        /**
         * Add your code here.
         */
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(gitObject);
        byte[] data = byteArrayOutputStream.toByteArray();
        File file=new File(path);
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        ZLibUtils.compress(data, fileOutputStream);

    }

}
