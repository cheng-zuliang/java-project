package gitobject;



import sha1.SHA1;

import zlib.ZLibUtils;
import fileoperation.FileReader;

import java.io.*;
import java.lang.reflect.Array;
import java.security.MessageDigest;
import java.util.*;

import static java.util.Collections.sort;


public class Tree extends GitObject{
    protected ArrayList<GitObject> treeList;	//GitObjects in tree

    public ArrayList<GitObject> getTreeList(){
        return treeList;
    }

    public void generateArrayList(File file,ArrayList<GitObject> list){
        File[] files=file.listFiles();
        assert files != null;
        for(File f:files){
            if(file.isFile()) {
                try {
                    list.add(new Blob(file));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if(file.isDirectory()) {
                Tree tree = null;
                try {
                    tree = new Tree(file);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                list.add(tree);
                generateArrayList(f, list);
            }
        }
    }
    public Tree(){}
    
    /**
     * Constructor
     * @param file
     * @throws Exception
     */

    public Tree(File file) throws Exception {
        ArrayList<GitObject> list=new ArrayList<>();
        //generateArrayList(file,list);
        this.treeList = list;
        this.fmt = "tree";
        this.mode = "040000";
        this.key=genKey(file);
        this.value = "040000 tree"+genKey(file)+" "+file.getName();

        //compressWrite(path);
    }
    /**
     * Deserialize a tree object with treeId and its path.
     * @param Id
     * @param Id
     * @throws IOException
     */
    public static Tree deserialize(String Id) throws IOException {
        try{
            /**
             * Todo: Add your code here.
             */
            String parentPath= path+File.separator+Id.substring(0,2);
            String filename=Id.substring(2);
            String path=parentPath+File.separator+filename;
            var obj=new ObjectInputStream(new FileInputStream(path));
            return (Tree) obj.readObject();
        }catch (Exception e){
            e.printStackTrace();
        }
        /* Todo: You should delete the return statement before you start. */
        return new Tree();//?不删除会发生错误么
    }


    /**
     * Sort the files in a certain order. You should consider whether it's a file or a directory.
     * @param fs
     * @return List
     */
    public static ArrayList<File> sortFile(File[] fs){
        ArrayList fileList = new ArrayList<File>(Arrays.asList(fs));
        sort(fileList, new Comparator<File>() {
            @Override //表示重写方法
            public int compare(File o1, File o2) {
                /* Todo: Add your code here. */
                if(o1.isDirectory() && o2.isFile()) {
                    return 1;
                }
                if(o1.isFile() && o2.isDirectory()) {
                    return -1;
                }
                else return o1.toString().compareTo(o2.toString());
            }
        });
        return fileList;
    }

    /**
     * Generate the key of a tree object.
     * @param dir
     * @return String
     * @throws Exception
     */
    public String genKey(File dir) throws Exception{
        /* Todo: Add your code here. */
        File[] fs =dir.listFiles();
        List<File> fileList=sortFile(fs);
        StringBuilder stringBuilder=new StringBuilder();
        for (File file : fileList) {
            if (file.isFile()) {
                stringBuilder.append(GitObject.getValue(file));
            }else if (file.isDirectory()){
                stringBuilder.append(genKey(new File(dir+File.separator+file.getName())));
            }
        }
        String value=stringBuilder.toString();
        String content="040000 tree"+" "+value;
        key= SHA1.getHash(content);
        return key;
    }

    @Override
    public String toString(){
        return "040000 tree " + key;
    }

}
